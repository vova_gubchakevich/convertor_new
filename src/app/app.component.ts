import {Component,OnInit} from '@angular/core';
import { Country,Countries } from './country/country';
import { CountryService } from './country/country.service';
@Component({
   moduleId: module.id,
  selector: 'my-app',
  templateUrl: './app.component.html', 
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
  name = 'Angular';
  countries=Countries;
  public selectedCountry:Country;
  selectedCountrySecond:Country;

  onSelect(country:Country):void{
    this.selectedCountry=country;
  }
  onSelectSecond(country:Country):void{
    this.selectedCountrySecond=country;
  }
  
}
