import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms'
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import {CurrencyComponent} from './currency/currency.component';
import {CountryComponent} from './country/country.component';
import {LoginComponent} from './login/login.component';
import { CountryService} from './country/country.service'
import {AuthenticationPage} from './authentication-page/authentication-page.component';
import { routing } from './app.routes';

@NgModule({
  imports:      [
     BrowserModule,
      FormsModule,
      HttpModule,
      JsonpModule,
    routing
      ],
  declarations: [ 
    AppComponent ,
    CurrencyComponent,
    CountryComponent,
    AuthenticationPage,
    LoginComponent
    ],
  bootstrap:    [ AppComponent ],
  providers:[
    CountryService
  ]
})
export class AppModule { }
