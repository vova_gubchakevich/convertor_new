import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';

import { Country, Countries } from './country';
import { CountryService } from './country.service';
import { Http } from '@angular/http';
import { AppComponent } from '../app.component'
@Component({
    moduleId: module.id,
    selector: 'country-show-detail',
    templateUrl: './country.conponent.html',
    styleUrls: ['./country.conponent.css']
})
export class CountryComponent {
    countries = Countries;
    selectedCountry: Country;
    selectedCountrySecond: Country;

    koefOfCurrency: string;
    count: number;
    totalSum: number;

    constructor(private service: CountryService) { }

    onSelect(country: Country): void {
        this.selectedCountry = country;
    }
    onSelectSecond(country: Country): void {
        this.selectedCountrySecond = country;
    }
    convert(): void {
        this.service
            .getCurrency(this.selectedCountry.name, this.selectedCountrySecond.name)
            .then((data) => {
                this.koefOfCurrency = (data[this.selectedCountrySecond.name.toUpperCase()]);
                this.totalSum = this.count * +this.koefOfCurrency;
                console.log('Curs-' + this.koefOfCurrency);
                console.log('Count-' + this.count);
                console.log('Count*curs-' + this.totalSum);
            });


        //total=count*koef
        //label=total
    }
}