import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Country } from './country';
import { AppComponent } from '../app.component';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class CountryService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private currencyUrl = 'http://api.fixer.io/latest?base=USD&symbols=RUB';  // URL to web api
    private from: string;
    private to: string;

    constructor(private http: Http) {
    }

    parseUrl(countryFirst: string, countrySecond: string): string {
        this.from = countryFirst.toUpperCase();
        this.to = countrySecond.toUpperCase();
        let base = 'http://api.fixer.io/latest?base=' + this.from + '&symbols=' + this.to;
        return base;
    }

    getCurrency(countryFirst: string, countrySecond: string): Promise<any> {
        this.currencyUrl = this.parseUrl(countryFirst, countrySecond);
        console.log('URL - ' + this.currencyUrl);

        if (countryFirst === countrySecond) {
            console.log('equals');
        }

        return this.http.get(this.currencyUrl)
            .toPromise()
            .then(response => response
            .json().rates);
    }

    getAllCurrencies(): Promise<Country[]> {
        return this.http.get(this.currencyUrl)
            .toPromise()
            .then(response => response
                .json().data as Country[]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}
