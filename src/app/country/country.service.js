"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var CountryService = (function () {
    function CountryService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.currencyUrl = 'http://api.fixer.io/latest?base=USD&symbols=RUB'; // URL to web api
    }
    CountryService.prototype.parseUrl = function (countryFirst, countrySecond) {
        this.from = countryFirst.toUpperCase();
        this.to = countrySecond.toUpperCase();
        var base = 'http://api.fixer.io/latest?base=' + this.from + '&symbols=' + this.to;
        return base;
    };
    CountryService.prototype.getCurrency = function (countryFirst, countrySecond) {
        this.currencyUrl = this.parseUrl(countryFirst, countrySecond);
        console.log('URL - ' + this.currencyUrl);
        if (countryFirst === countrySecond) {
            console.log('equals');
        }
        return this.http.get(this.currencyUrl)
            .toPromise()
            .then(function (response) { return response
            .json().rates; });
    };
    CountryService.prototype.getAllCurrencies = function () {
        return this.http.get(this.currencyUrl)
            .toPromise()
            .then(function (response) { return response
            .json().data; });
    };
    CountryService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return CountryService;
}());
CountryService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CountryService);
exports.CountryService = CountryService;
//# sourceMappingURL=country.service.js.map