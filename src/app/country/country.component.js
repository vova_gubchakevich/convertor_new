"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var country_1 = require("./country");
var country_service_1 = require("./country.service");
var CountryComponent = (function () {
    function CountryComponent(service) {
        this.service = service;
        this.countries = country_1.Countries;
    }
    CountryComponent.prototype.onSelect = function (country) {
        this.selectedCountry = country;
    };
    CountryComponent.prototype.onSelectSecond = function (country) {
        this.selectedCountrySecond = country;
    };
    CountryComponent.prototype.convert = function () {
        var _this = this;
        this.service
            .getCurrency(this.selectedCountry.name, this.selectedCountrySecond.name)
            .then(function (data) {
            _this.koefOfCurrency = (data[_this.selectedCountrySecond.name.toUpperCase()]);
            _this.totalSum = _this.count * +_this.koefOfCurrency;
            console.log('Curs-' + _this.koefOfCurrency);
            console.log('Count-' + _this.count);
            console.log('Count*curs-' + _this.totalSum);
        });
        //total=count*koef
        //label=total
    };
    return CountryComponent;
}());
CountryComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'country-show-detail',
        templateUrl: './country.conponent.html',
        styleUrls: ['./country.conponent.css']
    }),
    __metadata("design:paramtypes", [country_service_1.CountryService])
], CountryComponent);
exports.CountryComponent = CountryComponent;
//# sourceMappingURL=country.component.js.map