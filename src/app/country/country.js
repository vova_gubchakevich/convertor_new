"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Country = (function () {
    function Country() {
    }
    return Country;
}());
exports.Country = Country;
exports.Countries = [
    { name: 'uah', fullName: 'Grivna' },
    { name: 'usd', fullName: 'Dollar' },
    { name: 'eur', fullName: 'Evro' },
    { name: 'php', fullName: 'Philipinchik' },
    { name: 'aud', fullName: '' },
    { name: 'bgn', fullName: '' },
    { name: 'brl', fullName: '' },
    { name: 'cad', fullName: '' },
    { name: 'chf', fullName: '' },
    { name: 'cny', fullName: '' },
    { name: 'czk', fullName: '' },
    { name: 'dkk', fullName: '' },
    { name: 'gbp', fullName: '' },
    { name: 'hkd', fullName: '' },
    { name: 'hrk', fullName: '' },
    { name: 'huf', fullName: '' },
    { name: 'idr', fullName: '' },
    { name: 'ils', fullName: '' },
    { name: 'inr', fullName: '' },
    { name: 'jpy', fullName: '' },
    { name: 'krw', fullName: '' },
    { name: 'mxn', fullName: '' },
    { name: 'myr', fullName: '' },
    { name: 'nok', fullName: '' },
    { name: 'nzd', fullName: '' },
    { name: 'pln', fullName: '' },
    { name: 'ron', fullName: '' },
    { name: 'rub', fullName: '' },
    { name: 'sek', fullName: '' },
    { name: 'sgd', fullName: '' },
    { name: 'thb', fullName: '' },
    { name: 'try', fullName: '' },
    { name: 'zar', fullName: '' },
];
//# sourceMappingURL=country.js.map