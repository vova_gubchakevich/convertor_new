// Imports
// Deprecated import
// import { RouterConfig } from '@angular/router';
import { Routes } from '@angular/router';

import { CountryComponent }    from './country.component';


// Route Configuration
export const countryRoutes: Routes = [
  { path: 'country', component: CountryComponent },
  
];