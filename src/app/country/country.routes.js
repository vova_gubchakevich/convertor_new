"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var country_component_1 = require("./country.component");
// Route Configuration
exports.countryRoutes = [
    { path: 'country', component: country_component_1.CountryComponent },
];
//# sourceMappingURL=country.routes.js.map