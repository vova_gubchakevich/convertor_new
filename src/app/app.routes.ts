// Imports
// Deprecated import
// import { provideRouter, RouterConfig } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { countryRoutes }    from './country/country.routes';
import { AutPegeRoutes }    from './authentication-page/authentication-page.routes';
import { loginRoutes }    from './login/login.routes';

// Route Configuration
export const routes: Routes = [
  {
    path: '',
    redirectTo: '/autpege',
    pathMatch: 'full'
  },
  ...countryRoutes,
  ...AutPegeRoutes,
  ...loginRoutes
];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
