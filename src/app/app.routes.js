"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var country_routes_1 = require("./country/country.routes");
var authentication_page_routes_1 = require("./authentication-page/authentication-page.routes");
var login_routes_1 = require("./login/login.routes");
// Route Configuration
exports.routes = [
    {
        path: '',
        redirectTo: '/autpege',
        pathMatch: 'full'
    }
].concat(country_routes_1.countryRoutes, authentication_page_routes_1.AutPegeRoutes, login_routes_1.loginRoutes);
// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];
exports.routing = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routes.js.map