import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import {Image} from './image-interface';

@Component({
    moduleId: module.id,
    selector:"authentication-page",
    templateUrl: './authentication-page.component.html',
    styleUrls: ['./authentication-page.component.css']
})

export class AuthenticationPage{
    public images = IMAGES;
}

var IMAGES: Image[] = [
	{ "title": "Вартість Землі — п’ять квадрильйонів доларів", "url": "images/1.jpeg" },
	{ "title": "Існує купюра номіналом 2 долари . Однак , її вважають нещасливою , її відмовляються брати в якості зарплати або на здачу", "url": "images/2.jpg" },
	{ "title": "Штучні алмази вже не можна відрізнити від природних", "url": "images/4.jpg" }
];
