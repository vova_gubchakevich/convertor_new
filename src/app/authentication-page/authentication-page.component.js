"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AuthenticationPage = (function () {
    function AuthenticationPage() {
        this.images = IMAGES;
    }
    return AuthenticationPage;
}());
AuthenticationPage = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "authentication-page",
        templateUrl: './authentication-page.component.html',
        styleUrls: ['./authentication-page.component.css']
    })
], AuthenticationPage);
exports.AuthenticationPage = AuthenticationPage;
var IMAGES = [
    { "title": "Вартість Землі — п’ять квадрильйонів доларів", "url": "images/1.jpeg" },
    { "title": "Існує купюра номіналом 2 долари . Однак , її вважають нещасливою , її відмовляються брати в якості зарплати або на здачу", "url": "images/2.jpg" },
    { "title": "Штучні алмази вже не можна відрізнити від природних", "url": "images/4.jpg" }
];
//# sourceMappingURL=authentication-page.component.js.map