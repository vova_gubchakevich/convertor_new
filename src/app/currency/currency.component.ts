import { Component, Input } from '@angular/core'
import { Country, Countries } from '../country/country';
import { CountryService } from '../country/country.service';
import {  ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'currency-show-detail',
    templateUrl: './currency.component.html',
    styleUrls: ['./currency.component.css']
})
export class CurrencyComponent {
    @Input()
    firstCountry: Country;
    @Input()
    secondCountry:Country;
}